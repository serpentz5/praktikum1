"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url

from django.urls import re_path, path, include
from django.contrib import admin
from lab_1.views import index as index_lab1
from my_experience.views import my_experience
from guestbook.views import index as index_guestbook
import guestbook.urls as guestbook
from lab_4.views import index as index_lab4
from django.views.generic.base import TemplateView  # new


# Modul terakhir


from django.views.generic.base import RedirectView


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^my_experience/', include('my_experience.urls')),
    re_path(r'^$', index_lab1, name='index'),
    re_path(r'^my_experience/$', my_experience, name='my_experience'),
    re_path(r'^guestbook/', include('guestbook.urls')),
    re_path(r'^guestbook/$', index_guestbook, name='guestbook'),
    url(r'^guestbook/', include(guestbook, namespace='guestbook')),
    re_path(r'^lab_4/', include('lab_4.urls')),
    re_path(r'^lab_4/$', index_lab4, name='lab_4'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('home', TemplateView.as_view(
        template_name='home.html'), name='home'),  # new


]
