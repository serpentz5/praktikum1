from django.conf.urls import url
from .views import index

app_name = 'lab_4'
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
]
