from django.conf.urls import url
from .views import index, message_post, message_table
from django.urls import re_path


app_name = 'guestbook'

urlpatterns = [
    re_path(r'^add_message', message_post, name='add_message'),
    url(r'^$', index, name='index'),
    url(r'result_table', message_table, name='result_table'),
]
