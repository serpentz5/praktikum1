# Create your views here.
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message
from django.contrib.admin.views.decorators import staff_member_required

response = {'nama': "Nadhif"}


@staff_member_required
def index(request):
    response['message_form'] = Message_Form
    html = 'guestbook.html'
    return render(request, html, response)


def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'],
                          message=response['message'])
        message.save()
        html = 'form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/')


def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'table.html'
    return render(request, html, response)
